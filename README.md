# pystaticgen

testing several static site generators written in python

The compared generators are:
* [pelican](https://blog.getpelican.com/)
* [nikola](https://getnikola.com/)
* [tinkerer](http://tinkerer.me/)

Pelican looks good, but merging translations by slug or title doesn't seem very handy.
Btw, there is lots of plugins available.
Pelican's default theme font does not support russian glyphs.

Nikola still is the favourite. In comparison to Pelican at least.
It deduces translations from post file name, based on mask.
Demo-site works with russian like a charm. And lots of plugins hang around, too.

Tinkerer looks like a very basic thing. Can't see the way to write translatable posts.
It's strongly based on Sphinx. Maybe its i18n should be used.
1.6 release happened more than 2 years ago and community is pretty small.
Seems like a poor choice after all.