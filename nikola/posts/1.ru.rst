.. title: Добро пожаловать в "Никола"!
.. slug: welcome-to-nikola
.. date: 2012-03-30 23:00:00 UTC-03:00
.. tags: nikola, python, demo, blog
.. author: Den U
.. link: https://getnikola.com/
.. description:
.. category: nikola

.. figure:: https://farm1.staticflickr.com/138/352972944_4f9d568680.jpg
   :target: https://farm1.staticflickr.com/138/352972944_4f9d568680_z.jpg?zz=1
   :class: thumbnail
   :alt: Угол Никола Тесла by nicwest, on Flickr

Если получилось читать это в браузере, значит вы установили "Никола"
и построили сайт на его основе. Поздравляем!

Теперь можно:

* :doc:`Прочесть руководство <handbook>`
* `Посетить сайт "Никола" для погружения <https://getnikola.com>`__
* `Посмотреть демо фотогалереи <link://gallery/demo>`__
* :doc:`See a demo listing <listings-demo>`
* :doc:`Посмотреть пример длиннотекста <dr-nikolas-vendetta>`

Делитесь впечатлениями по адресу info@getnikola.com!
